import storage from 'react-native-simple-store';
import axios from 'axios';

const BASE_URL = 'https://devapp.popepper.com.br/api/';
const VERSION = '1.3.7';
const COMPANY_TOKEN = '2ZBXYYY5ZA';

export const API = {
    async get(URI) {
        var axiosOptions = {
			method: 'GET',
			url: BASE_URL + URI,
			headers: {
				'Version': VERSION,
				'Company-Token': COMPANY_TOKEN
			},
			json: true
		};

        return new Promise(function (resolve, reject) {
            axios(axiosOptions).then(function(response) {
                console.log(response);


                if (response.data.success) {
                    resolve(response.data.data);
                } else {
                    alert(response.data.msg);
                }
            }).catch(function(error) {
                alert('Error ao realizar GET!');
                console.log(error);
            });
        })

    },
    post() {

    }
}
