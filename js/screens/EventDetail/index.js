import React, { Component } from "react";
import { Image } from "react-native";
import {
	Container,
	Text,
	View,
	Toast,
	Content,
	Button,
	Header,
	Title,
	Left,
	Body,
	Right,
	Icon,
	Card,
	CardItem,
	List,
	ListItem
} from "native-base";

import styles from "./styles";
import axios from 'axios';
import HTMLView from 'react-native-htmlview';

import { API } from '../../Api';

export default class EventDetail extends Component {

	constructor(props) {
		super(props);

		this.state = {
			eventDetail: {
				dates: [
					{ tickets: [] }
				]
			}
		}
	}


	componentWillMount() {
		let self = this;
		let event_id = this.props.navigation.state.params.id;

		API.get('event/' + event_id).then(function (response) {
			console.log(response);
			let htmlView = `<p><strong>P&Ocirc;R DO SAMBA</strong></p>
			<p>O melhor domingo do Brasil acontece aqui no Colosso com o nosso P&ocirc;r do Samba.<br />
			Corre pra garantir teu ingresso.</p>
			<p><strong>Atra&ccedil;&otilde;es:</strong><br />
			- Marcinho<br />
			- S&atilde;o 2<br />
			- Coletivo P&ocirc;r do Samba<br />
			- Balan&ccedil;o Social<br />
			- DJ Thiago Camargo</p>
			<p><strong>Reservas VIP:</strong><br />
			(85) 9 8160.0088<br />
			reservas@colossofortaleza.com.br</p>
			<p><strong>Informa&ccedil;&otilde;es Gerais:</strong><br />
			(85) 9 8160.0088<br />
			@colossofortaleza<br />
			www.colossofortaleza.com.br</p>`;

			// response.description = htmlView;

			self.setState({
				eventDetail: response
			})

		});
	}

	render() {
		return (
			<Container>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Icon name="arrow-back" />
						</Button>
					</Left>
					<Body>
						<Title>Nossas Festas</Title>
					</Body>
					<Right>
						<Button
							transparent
							onPress={() => this.props.navigation.navigate("DrawerOpen")}>
							<Icon name="menu" />
						</Button>
					</Right>
				</Header>
				<Content>
					<Image style={{ width: 375, height: 187, padding: 0, margin: 0 }}
						source={{ uri: this.state.eventDetail.banner_app }} />
					<List>
					{
                    	this.state.eventDetail.dates[0].tickets.map((itemTicket, indexTicket) => (
							<View key={indexTicket}>
								<ListItem itemDivider>
									<Text>{ itemTicket.name }</Text>
								</ListItem>
								{
									itemTicket.lot.map((itemLot, indexLot) => (
										<ListItem key={indexLot}>
											<Text>{ itemLot.name }</Text>
										</ListItem>
									))
								}
							</View>
						))
					}
						<ListItem itemDivider>
							<Text>{ this.state.eventDetail.dates[0].date }</Text>
						</ListItem>
						<HTMLView
							addLineBreaks={false}
        					value={ this.state.eventDetail.description }/>
					</List>
				</Content>
			</Container>
		);
	}
}
