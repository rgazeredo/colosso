import React, { Component } from "react";
import { Image } from "react-native";
import {
	Container,
	Text,
	View,
	Toast,
	Content,
	Button,
	Header,
	Title,
	Left,
	Body,
	Right,
	Icon,
	Card,
	CardItem,
	H3
} from "native-base";

import { API } from '../../Api';

import styles from "./styles";

export default class Home extends Component {

	constructor(props) {
		super(props);

		this.state = {
			listEvents: []
		}
	}


	componentWillMount() {
		var self = this;

		API.get('events').then(function (response) {
			self.setState({
				listEvents: response.events
			})
		});
	}

	openEventDetail(item) {
		this.props.screenProps.rootNavigation.navigate('EventDetail', {id: item.id});
	}

	render() {
		return (
			<Container>
				<Header>
					<Left>
					</Left>
					<Body>
						<Title>Nossas Festas</Title>
					</Body>
					<Right>
						<Button
							transparent
							onPress={() => this.props.navigation.navigate("DrawerOpen")}>
							<Icon name="menu" />
						</Button>
					</Right>
				</Header>
				<Content>
				{
                    this.state.listEvents.map((item, index) => (
					<Card key={index}>
						<CardItem button onPress={() => this.openEventDetail(item)}>
							<Body>
								<Image style={{ width: 375, height: 187, padding: 0, margin: 0 }}
									source={{uri: item.cover_app}}/>
								<Text>
									{item.name}
								</Text>
							</Body>
						</CardItem>
					</Card>
					))
				}
				</Content>
			</Container>
		);
	}
}
