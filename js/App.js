import React from "react";
import { StackNavigator } from "react-navigation";
import { Root } from "native-base";

import SideBar from "./components/Sidebar";
import Home from "./screens/Home";
import EventDetail from "./screens/EventDetail";
// import Login from "./screens/Login";
import Drawer from "./Drawer";
// import Card from "./screens/Card";
Drawer.navigationOptions = ({ navigation }) => ({
	header: null
});
const AppNavigator = StackNavigator(
	{
		// Login: { screen: Login },
		Home: { screen: Home },
		EventDetail: { screen: EventDetail },
		// Card: { screen: Card },
		Drawer: { screen: ({ navigation }) => <Drawer screenProps={{ rootNavigation: navigation }} /> }
	},
	{
		index: 0,
		initialRouteName: "Drawer",
		headerMode: "none"
	}
);

export default () =>
	<Root>
		<AppNavigator />
	</Root>;
